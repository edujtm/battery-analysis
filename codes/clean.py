import pandas as pd

HEALTH_CATEGORIES = ['GOOD', 'COLD', 'OVERHEAT', 'DEATH', 'OVER VOLTAGE', 'UNSPECIFIED FAILURE', 'UNKNOWN']
STATUS_CATEGORIES = ['CHARGING', 'FULL', 'DISCHARGING', 'NOT CHARGING', 'UNKNOWN']


class InvalidArgumentException(Exception):
    def __init__(self, message):
        super(InvalidArgumentException, self).__init__(message)


def assert_columns(dataframe: pd.DataFrame) -> None:
    expected_columns = {'timestamp', 'current_now', 'current_avg', 'voltage_now', 'voltage_avg', 'batt_temp',
                        'health', 'status'}
    clmns = dataframe.columns.tolist()

    if not set(clmns).issuperset(expected_columns):
        raise InvalidArgumentException('Expected dataframe to have the following columns: {0}'.format(expected_columns))


def clean_energy_data(dataframe: pd.DataFrame) -> pd.DataFrame:
    """
    This function will transform the data from the battery reader into a cleaner format
    using pandas types. This is mainly a helper function to parse csv files from multiple
    experiments.

    It'll parse the timestamp column and set it as index.
    It'll create a UTC datetime column from the timestamp
    Change the health and status columns to categorical values.
    Transform the current readings from uA to mA.
    Transform the voltage readings from uV to V.
    Transform the temperature readings from tenths of centigrate to centigrates

    :param dataframe: A dataframe with the information read from the battery reader log file
    :return: A new dataframe with clean data
    """

    assert_columns(dataframe)

    clean_data = dataframe.copy()
    clean_data['datetime'] = clean_data['timestamp'].apply(lambda x: pd.to_datetime(x, unit='ms'))
    clean_data.set_index('datetime', inplace=True)

    clean_data['health'] = pd.Categorical(clean_data['health'], categories=HEALTH_CATEGORIES)
    clean_data['status'] = pd.Categorical(clean_data['status'], categories=STATUS_CATEGORIES)

    # Transformation current from uA to mA
    # Transform voltage from uV to V
    # Transform temperature from tenths of centigrate to centigrate (° C)
    clean_data[['current_now', 'current_avg']] = clean_data[['current_now', 'current_avg']].apply(lambda x: x / 1e3)
    clean_data[['voltage_now', 'voltage_avg']] = clean_data[['voltage_now', 'voltage_avg']].apply(lambda x: x / 1e6)
    clean_data['batt_temp'] = clean_data['batt_temp'].apply(lambda x: x / 10)
    clean_data['power'] = ((clean_data['current_now'] * -1) / 1e3) * clean_data['voltage_now']
    return clean_data
