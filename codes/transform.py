import pandas as pd
from codes.clean import assert_columns

SECONDS_IN_HOUR = 60 * 60

# This function is probably wrong. Need to review it or use numpy.trapz
def calculate_energy_consumption(dataframe: pd.DataFrame) -> float:
    """
    Calculates the energy consumption in Kwh from the dataframe returned by
    clean_energy_data.

    It expects to have a voltage_now column with units in Volts,
    a current_now column with units in milliAmperes and a timestamp
    column with UNIX timestamps in seconds since the UNIX epoch.

    :param dataframe: Cleaned dataframe with battery energy readings
    :return: The energy consumption in Kwh (Kilowatts-Hour)
    """

    assert_columns(dataframe)

    # TODO check for units in each column to see if they result in Watts
    current_amp = dataframe['current_now'] / 1000
    power = current_amp.multiply(dataframe['voltage_now'], fill_value=0.0)
    avg_power = power.sum() / power.size

    # The timestamp is given in seconds since 1970-01-01 00:00:00 UTC
    time_in_hours = (dataframe.iloc[-1]['timestamp'] - dataframe.iloc[0]['timestamp']) / SECONDS_IN_HOUR
    return avg_power * time_in_hours

def remove_delay(dataframe: pd.DataFrame) -> pd.DataFrame:
    """
    Since the experiments are made at different times, the timestamps are displaced between experiments
    so when plotting them in superposition they are seen at different points in the time scale (x-axis).
    
    This functions transforms the index from timestamps to the elapsed time from the start of application
    for each sample.
    
    This also allows for integration with numpy.trapz
    
    :param dataframe: TODO
    :return: TODO
    """
    # Avoid changing the original dataframe
    datacopy = dataframe.copy()
    
    # Removes the starting time from the timestamp so that they become normalized, allowing for comparison
    # The timestamps are UNIX epoch times measured in milliseconds
    datacopy['timestamp'] = dataframe['timestamp'].apply(lambda x: x - dataframe.iloc[0]['timestamp'])
    
    return datacopy.set_index('timestamp')